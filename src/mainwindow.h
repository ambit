/**
 * Copyright (C) 2007 Benjamin C. Meyer
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <qmainwindow.h>
#include <qabstractitemmodel.h>

class QSidebar;
class View;
class QSplitter;

class QToolBar;
class QAbstractItemView;
class QActionGroup;
class QMenuBar;
class History;
class QLabel;
class QFileSystemModel;

class MainWindow : public QMainWindow
{

    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void setRootIndex(const QModelIndex &index);
    void goUp();
    void showAbout();

private slots:
    void showView(QAction *action);
    void arrangeBy(QAction *action);

private:
    void createMenu();
    void createActions();
    void createToolBar();
    void createWidgets();

    QFileSystemModel *model;

    QSplitter *splitter;
    QSidebar *sidebar;
    View *view;

    QActionGroup *viewActionsGroup;
    QActionGroup *historyActionsGroup;
    QActionGroup *arrangeByActionGroup;
    QToolBar *toolBar;
    QLabel *statusBarLabel;

    QMenu *fileMenu;
    QMenu *editMenu;
    QMenu *viewMenu;
    QMenu *goMenu;
    QMenu *windowMenu;
    QMenu *helpMenu;

    QAction *fileOpenAction;
    QAction *fileCloseAction;
    QAction *fileQuitAction;
    QAction *backAction;
    QAction *forwardAction;
    QAction *goUpAction;
    QAction *minimizeWindowAction;
    QAction *about;
};

#endif // MAINWINDOW_H

