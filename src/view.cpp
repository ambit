/**
 * Copyright (C) 2007 Benjamin C. Meyer
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "view.h"

#include "iconview.h"
#include "treeview.h"
#include "columnview.h"
#include "history.h"
#include "qfilesystemmodel_p.h"

#include <QtGui/QtGui>

View::View(QWidget *parent) : QStackedWidget(parent), model(0)
{
    history = new History(this);
    connect(this, SIGNAL(currentFolderChanged(const QModelIndex &)),
            history, SLOT(currentChanged(const QModelIndex &)));
    connect(history, SIGNAL(goToIndex(const QModelIndex &)),
            this, SLOT(setRootIndex(const QModelIndex &)));
}

View::~View()
{}

void View::createViews(QFileSystemModel *model)
{
    this->model = model;
    IconView *iconView = new IconView(this);
    addView(iconView);
    TreeView *treeView = new TreeView(this);
    addView(treeView);
    ColumnView *columnView = new ColumnView(this);
    addView(columnView);
}

QModelIndex View::rootIndex() const
{
    if (!model)
        return QModelIndex();
    return currentView()->rootIndex();
}

void View::setCurrentMode(ViewMode mode)
{
    if (!views.isEmpty())
        setCurrentWidget(views.at((int)mode));
}

View::ViewMode View::currentMode() const
{
    if (currentIndex() > 0)
        return (ViewMode)(currentIndex());
    return Icon;
}

QAbstractItemView *View::currentView() const
{
    if (views.isEmpty())
        return 0;
    return views.at(currentIndex());
}

void View::fileOpen()
{
    if (!model)
        return;
    QModelIndex index = currentView()->currentIndex();
    if (!index.isValid())
        return;
    if (model->hasChildren(index)) {
        QMetaObject::invokeMethod(currentView(), "openIndex", Q_ARG(QModelIndex, index));
        // TODO root index might change
        return;
    }

    QUrl url = QUrl::fromLocalFile(index.data(QFileSystemModel::FilePathRole).toString());
    QDesktopServices::openUrl(url);
}

void View::setRootIndex(const QModelIndex &index)
{
    if (!model)
        return;
    if (rootIndex() == index) {
        qWarning() << index << "is the current root";
        return;
    }

    if (!model->hasChildren(index)) {
        fileOpen();
        return;
    }

    for (int i = 0; i < views.count(); ++i) {
        views.at(i)->setRootIndex(index);
    }

    emit currentFolderChanged(index);
}

void View::goUp()
{
    QAbstractItemView *view = currentView();
    if (!view)
        return;
    QModelIndex current = view->currentIndex();
    QModelIndex root = view->rootIndex();
    if (current.isValid() && current.parent() != root)
        root = current;
    root = root.parent();
    view->setCurrentIndex(root);
    if (view->visualRect(root).isNull())
        view->setRootIndex(root);
}

void View::addView(QAbstractItemView *view)
{
    views.append(view);
    view->setSelectionBehavior(QAbstractItemView::SelectRows);
    view->setModel(model);
    addWidget(view);

    if (!views.isEmpty())
        view->setSelectionModel(views.first()->selectionModel());
    connect(view, SIGNAL(doubleClicked(const QModelIndex &)),
            this, SLOT(fileOpen()));
}
