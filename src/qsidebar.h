/****************************************************************************
**
** Copyright (C) 1992-2007 Trolltech ASA. All rights reserved.
**
** This file is part of the QtGui module of the Qt Toolkit.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 as published by the Free Software Foundation
** and appearing in the file LICENSE.GPL included in the packaging of
** this file.  Please review the following information to ensure GNU
** General Public Licensing requirements will be met:
** http://www.trolltech.com/products/qt/opensource.html
**
** If you are unsure which license is appropriate for your use, please
** review the following information:
** http://www.trolltech.com/products/qt/licensing.html or contact the
** sales department at sales@trolltech.com.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/


#ifndef QSIDEBAR_H
#define QSIDEBAR_H

#include <qlistwidget.h>
#include <qstandarditemmodel.h>
#include <qurl.h>

#ifndef QT_NO_FILEDIALOG

class QFileSystemModel;
class Q_AUTOTEST_EXPORT QUrlModel : public QStandardItemModel
{
    Q_OBJECT

public:
    enum Roles {
        UrlRole = Qt::UserRole + 1,
    };

    QUrlModel(QObject *parent = 0);

    QStringList mimeTypes() const;
    QMimeData *mimeData(const QModelIndexList &indexes) const;
    bool canDrop(QDragEnterEvent *event);
    bool dropMimeData(const QMimeData *data, Qt::DropAction action, int row, int column, const QModelIndex &parent);
    Qt::ItemFlags flags(const QModelIndex &index) const;
    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole);

    void setUrls(const QList<QUrl> &list);
    void addUrls(const QList<QUrl> &urls, int row = -1, bool move = true);
    QList<QUrl> urls() const;
    void setFileSystemModel(QFileSystemModel *model);
    QFileSystemModel *qFileSystemModel() const
    {
        return fileSystemModel;
    }
private Q_SLOTS:
    void dataChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight);
    void layoutChanged();

private:
    void setUrl(const QModelIndex &index, const QUrl &url, const QModelIndex &dirIndex);
    void changed(const QString &path);
    void addIndexToWatch(const QString &path, const QModelIndex &index);
    QFileSystemModel *fileSystemModel;
    QList<QPair<QModelIndex, QString> > watching;
    QList<QUrl> invalidUrls;
};

class Q_AUTOTEST_EXPORT QSidebar : public QListView
{
    Q_OBJECT

Q_SIGNALS:
    void goToUrl(const QUrl &url);
    void goTo(const QModelIndex &index);

public:
    QSidebar(QFileSystemModel *model, const QList<QUrl> &newUrls, QWidget *parent = 0);
    ~QSidebar();

    QSize sizeHint() const;

    void setUrls(const QList<QUrl> &list)
    {
        urlModel->setUrls(list);
    }
    void addUrls(const QList<QUrl> &list, int row)
    {
        urlModel->addUrls(list, row);
    }
    QList<QUrl> urls() const
    {
        return urlModel->urls();
    }

    void selectUrl(const QUrl &url);

public slots:
    void selectIndex(const QModelIndex &index);

protected:
    void focusInEvent(QFocusEvent *event);
    void dragEnterEvent(QDragEnterEvent *event);

private Q_SLOTS:
    void clicked(const QModelIndex &index);
    void showContextMenu(const QPoint &position);
    void removeEntry();

private:
    QUrlModel *urlModel;
};

#endif

#endif

