/**
 * Copyright (C) 2007 Benjamin C. Meyer
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef HISTORY_H
#define HISTORY_H

#include <qobject.h>
#include <qabstractitemmodel.h>
#include <qlist.h>

class QAction;

class History : public QObject
{

    Q_OBJECT

signals:
    void goToIndex(const QModelIndex &index);

public:
    History(QObject *parent = 0);
    ~History();

    static QAction *backAction(QObject *parent);
    static QAction *forwardAction(QObject *parent);

    QAction *goForwardAction;
    QAction *goBackAction;

public slots:
    void goBack();
    void goForward();
    void currentChanged(const QModelIndex &index);

private:
    QList<QPersistentModelIndex> history;
    int currentLocation;
};

#endif // MAINWINDOW_H

