TEMPLATE = app
TARGET = ambit

mac {
    TARGET =Ambit
}

DEPENDPATH += .
INCLUDEPATH += .
RESOURCES += ambit.qrc
DESTDIR = ../
OBJECTS_DIR = build
MOC_DIR = build
UI_DIR = build
QT += script

CONFIG += warn_on debug

# Input
HEADERS += mainwindow.h \
	view.h \
	treeview.h \
	history.h \
	iconview.h \
	qsidebar.h \
	columnview.h \
	diskinformation.h \
	aboutdialog.h \
	qfilesystemmodel_p.h \
	qfileinfogatherer_p.h

SOURCES += main.cpp \
	view.cpp \
	mainwindow.cpp \
	treeview.cpp \
	history.cpp \
	iconview.cpp \
	qsidebar.cpp \
	columnview.cpp \
	diskinformation.cpp \
	aboutdialog.cpp\
	qfilesystemmodel.cpp \
	qfileinfogatherer.cpp
