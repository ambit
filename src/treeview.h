/**
 * Copyright (C) 2007 Benjamin C. Meyer
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef TREEVIEW_H
#define TREEVIEW_H

#include <qtreeview.h>

class TreeView : public QTreeView
{

    Q_OBJECT

public slots:
    void openIndex(const QModelIndex &index)
    {
        scrollTo(index);
        expand(index);
        if (visualRect(index).isNull())
            setRootIndex(index);
    }

public:
    TreeView(QWidget *parent = 0);
    ~TreeView();
    void setModel(QAbstractItemModel * model);

};

#endif // TREEVIEW_H

