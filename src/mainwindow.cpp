/**
 * Copyright (C) 2007 Benjamin C. Meyer
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "mainwindow.h"

#include "view.h"
#include "aboutdialog.h"

#include "history.h"
#include "qsidebar.h"
#include "diskinformation.h"
#include "qfilesystemmodel_p.h"

#include <QtGui/QtGui>

MainWindow::MainWindow(QWidget * parent) : QMainWindow(parent), model(0)
{
    //setAttribute(Qt::WA_MacMetalStyle, true);
    model = new QFileSystemModel(this);
    model->setReadOnly(false);

    createWidgets();
    createActions();
    createToolBar();
    createMenu();

    setRootIndex(model->index(QDir::homePath()));
}

MainWindow::~MainWindow()
{}

void MainWindow::createWidgets()
{
    toolBar = addToolBar(QLatin1String("Toolbar"));

    splitter = new QSplitter(this);

    QList<QUrl> initialList;
    initialList << QUrl::fromLocalFile("/");
    initialList << QUrl::fromLocalFile(QDir::homePath() + "/Desktop");
    initialList << QUrl::fromLocalFile(QDir::homePath());
#ifdef Q_OS_MAC
    initialList << QUrl::fromLocalFile("/Applications");
#endif
    initialList << QUrl::fromLocalFile(QDir::homePath() + "/Documents");
    initialList << QUrl::fromLocalFile(QDir::homePath() + "/Movies");
    initialList << QUrl::fromLocalFile(QDir::homePath() + "/Music");
    initialList << QUrl::fromLocalFile(QDir::homePath() + "/Pictures");
    sidebar = new QSidebar(model, initialList, this);
    connect(sidebar, SIGNAL(goTo(const QModelIndex &)),
            this, SLOT(setRootIndex(const QModelIndex &)));

    view = new View(this);
    view->createViews(model);
    connect(view, SIGNAL(currentFolderChanged(const QModelIndex &)),
            sidebar, SLOT(selectIndex(const QModelIndex &)));

    splitter->addWidget(sidebar);
    splitter->addWidget(view);
    splitter->setStretchFactor(splitter->indexOf(view), QSizePolicy::Expanding);
    setCentralWidget(splitter);

    historyActionsGroup = new QActionGroup(this);
    historyActionsGroup->setExclusive(true);

    viewActionsGroup = new QActionGroup(this);
    viewActionsGroup->setExclusive(true);
    connect(viewActionsGroup, SIGNAL(triggered(QAction *)), this, SLOT(showView(QAction *)));

    statusBarLabel = new QLabel(statusBar());
    statusBarLabel->setAlignment(Qt::AlignCenter);
    statusBar()->addWidget(statusBarLabel, 1);
}

void MainWindow::createActions()
{
    about = new QAction(tr("About ") + qApp->applicationName(), this);
    about->setIcon(qApp->windowIcon());
    about->setMenuRole(QAction::AboutRole);
    connect(about, SIGNAL(triggered()), this, SLOT(showAbout()));

    fileOpenAction = new QAction(tr("Open"), this);
    fileOpenAction->setShortcut(tr("Ctrl+O"));
    connect(fileOpenAction, SIGNAL(triggered()), view, SLOT(fileOpen()));

    fileCloseAction = new QAction(tr("Close Window"), this);
    fileCloseAction->setShortcut(tr("Ctrl+W"));
    connect(fileCloseAction, SIGNAL(triggered()), this, SLOT(close()));

    fileQuitAction = new QAction(tr("Quit"), this);
    fileQuitAction->setShortcut(tr("Ctrl+Q"));
    connect(fileQuitAction, SIGNAL(triggered()), qApp, SLOT(quit()));

    QAction *iconAction = new QAction(QIcon(":/pics/iconview.png"), "as Icons", this);
    viewActionsGroup->addAction(iconAction);
    iconAction->setCheckable(true);
    iconAction->setShortcut((QString("Ctrl+%1").arg(viewActionsGroup->actions().count())));
    iconAction->setChecked(true);

    QAction *treeAction = new QAction(QIcon(":/pics/treeview.png"), "as List", this);
    viewActionsGroup->addAction(treeAction);
    treeAction->setCheckable(true);
    treeAction->setShortcut((QString("Ctrl+%1").arg(viewActionsGroup->actions().count())));

    QAction *columnAction = new QAction(QIcon(":/pics/columnview.png"), "as Columns", this);
    viewActionsGroup->addAction(columnAction);
    columnAction->setCheckable(true);
    columnAction->setShortcut((QString("Ctrl+%1").arg(viewActionsGroup->actions().count())));

    goUpAction = new QAction(tr("Enclosing Folder"), toolBar);
    goUpAction->setShortcut(Qt::CTRL + Qt::Key_Up);
    goUpAction->setIcon(style()->standardIcon(QStyle::SP_ArrowUp));
    goUpAction->setEnabled(false);
    connect(goUpAction, SIGNAL(triggered()), this, SLOT(goUp()));

    minimizeWindowAction = new QAction(tr("&Minimize"), this);
    minimizeWindowAction->setShortcut(tr("Ctrl+M"));
    connect(minimizeWindowAction, SIGNAL(triggered()), this, SLOT(showMinimized()));
    // TODO disable minimize when minimized

    QAction *action = toolBar->toggleViewAction();
    action->setShortcut(Qt::CTRL + Qt::Key_T + Qt::ALT);
    // TODO text should say "show" or "hide"

    backAction = History::backAction(this);
    connect(backAction, SIGNAL(triggered()), view->history, SLOT(goBack()));
    forwardAction = History::forwardAction(this);
    connect(forwardAction, SIGNAL(triggered()), view->history, SLOT(goForward()));
    view->history->goBackAction = backAction;
    view->history->goForwardAction = forwardAction;
}

void MainWindow::arrangeBy(QAction *action)
{
    model->sort(arrangeByActionGroup->actions().indexOf(action), Qt::AscendingOrder);
}

void MainWindow::createMenu()
{
    fileMenu = menuBar()->addMenu(tr("&File"));
    fileMenu->addAction(fileOpenAction);
    fileMenu->addAction(fileCloseAction);
    fileMenu->addAction(fileQuitAction);

    editMenu = menuBar()->addMenu(tr("&Edit"));

    // View
    viewMenu = menuBar()->addMenu(tr("&View"));
    for (int i = 0; i < viewActionsGroup->actions().count(); ++i)
        viewMenu->addAction(viewActionsGroup->actions().at(i));
    viewMenu->addSeparator();

    QMenu *arrangeByMenu = viewMenu->addMenu(tr("Arrange By"));
    connect(arrangeByMenu, SIGNAL(triggered(QAction *)),
            this, SLOT(arrangeBy(QAction *)));

    arrangeByActionGroup = new QActionGroup(this);
    for (int i = 0; i < model->columnCount(); ++i)
        new QAction(model->headerData(i, Qt::Horizontal, Qt::DisplayRole).toString(), arrangeByActionGroup);
    arrangeByMenu->addActions(arrangeByActionGroup->actions());

    viewMenu->addSeparator();
    viewMenu->addAction(toolBar->toggleViewAction());

    // Go
    goMenu = menuBar()->addMenu(tr("Go"));
    goMenu->addAction(backAction);
    goMenu->addAction(forwardAction);
    goMenu->addAction(goUpAction);
    goMenu->addSeparator();

    windowMenu = menuBar()->addMenu(tr("Window"));
    windowMenu->addAction(minimizeWindowAction);

    helpMenu = menuBar()->addMenu(tr("Help"));
    helpMenu->addAction(about);
}

void MainWindow::createToolBar()
{
    toolBar->setMovable(false);
    toolBar->addAction(backAction);
    toolBar->addAction(forwardAction);
    for (int i = 0; i < viewActionsGroup->actions().count(); ++i)
        toolBar->addAction(viewActionsGroup->actions().at(i));
    setUnifiedTitleAndToolBarOnMac(true);
}

void MainWindow::showView(QAction *action)
{
    for (int i = 0; i < viewActionsGroup->actions().count(); ++i) {
        if (viewActionsGroup->actions().at(i) == action) {
            view->setCurrentMode((View::ViewMode)(i));
            break;
        }
    }
}

void MainWindow::goUp()
{
    view->goUp();
}

void MainWindow::setRootIndex(const QModelIndex &index)
{
    view->setRootIndex(index);
    goUpAction->setEnabled(index.parent().isValid());
    setWindowIcon(index.data(Qt::DecorationRole).value<QIcon>());
    setWindowTitle(index.data().toString());
    // update statusbar
    qint64 freeSpace = getDiskInformation(index.data(QFileSystemModel::FilePathRole).toString()).free;
    QString freeSpaceString = humanSize(freeSpace);
    statusBarLabel->setText(QString("%1 available").arg(freeSpaceString));
}

void MainWindow::showAbout()
{
    AboutDialog *aboutDialog = new AboutDialog(this);
    aboutDialog->setVersion("Version 0.1");
    QStringList authors;
    authors << "� 2007 Benjamin C Meyer <a href=\"mailto:ben@meyerhome.net\">ben@meyerhome.net</a>";
    aboutDialog->addAuthors(authors);
    aboutDialog->show();
}
