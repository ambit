/**
 * Copyright (C) 2007 Benjamin C. Meyer
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef VIEW_H
#define VIEW_H

#include <qstackedwidget.h>
#include <qabstractitemview.h>

class QFileSystemModel;
class History;

class View : public QStackedWidget
{

    Q_OBJECT

signals:
    void currentFolderChanged(const QModelIndex &index);

public:
    enum ViewMode
    {
        Icon = 0,
        List,
        Column
    };

    View(QWidget *parent = 0);
    ~View();

    void createViews(QFileSystemModel *model);

    void setCurrentMode(ViewMode mode);
    ViewMode currentMode() const;

    QModelIndex rootIndex() const;
    QAbstractItemView *currentView() const;
    History *history;

public slots:
    void setRootIndex(const QModelIndex &index);
    void goUp();
    void fileOpen();

private:
    void addView(QAbstractItemView *view);
    QFileSystemModel *model;
    QList<QAbstractItemView *> views;
};

#endif // VIEW_H

