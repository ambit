/**
 * Copyright (C) 2007 Benjamin C. Meyer
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "treeview.h"
#include <qheaderview.h>

TreeView::TreeView(QWidget *parent) : QTreeView(parent)
{
    setSortingEnabled(true);
    header()->setSortIndicator(0, Qt::AscendingOrder);
    setEditTriggers(QAbstractItemView::EditKeyPressed);
    setContextMenuPolicy(Qt::CustomContextMenu);
}

TreeView::~TreeView()
{}

void TreeView::setModel(QAbstractItemModel *model)
{
    QTreeView::setModel(model);
    header()->resizeSection(0, 250);
}

