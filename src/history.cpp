/**
 * Copyright (C) 2007 Benjamin C. Meyer
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "history.h"

#include <qaction.h>
#include <qstyle.h>
#include <qdebug.h>

History::History(QObject *parent) : QObject(parent), goForwardAction(0), goBackAction(0), currentLocation(-1)
{}

History::~History()
{}

QAction *History::backAction(QObject *parent)
{
    QAction *action = new QAction(tr("Back"), parent);
    action->setShortcuts(QKeySequence::Back);
    action->setIcon(QIcon(":/pics/arrow-left.png"));
    action->setEnabled(false);
    action->setToolTip(tr("Back"));
    return action;
}

QAction *History::forwardAction(QObject *parent)
{
    QAction *action = new QAction(tr("Forward"), parent);
    action->setShortcuts(QKeySequence::Forward);
    action->setIcon(QIcon(":/pics/arrow-right.png"));
    action->setEnabled(false);
    action->setToolTip(tr("Forward"));
    return action;
}

void History::currentChanged(const QModelIndex &index)
{
    if (currentLocation < 0 || history.value(currentLocation) != index) {
        while (currentLocation >= 0 && currentLocation + 1 < history.count()) {
            history.removeLast();
        }
        history.append(index);
        ++currentLocation;
    }
    if (goForwardAction)
        goForwardAction->setEnabled(history.size() - currentLocation > 1);
    if (goBackAction)
        goBackAction->setEnabled(currentLocation > 0);
}

void History::goBack()
{
    if (!history.isEmpty() && currentLocation > 0) {
        --currentLocation;
        QModelIndex previousIndex = history.at(currentLocation);
        emit goToIndex(previousIndex);
    }
}

void History::goForward()
{
    if (!history.isEmpty() && currentLocation < history.size()) {
        ++currentLocation;
        QModelIndex nextIndex = history.at(currentLocation);
        emit goToIndex(nextIndex);
    }
}

