/**
 * Copyright (C) 2007 Benjamin C. Meyer
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <QtTest/QtTest>
#include <QtGui/QtGui>

#include "history.h"

class HistoryTest : public QObject
{
    Q_OBJECT

private slots:
    void empty();
    void back();
    void forward();
};

void HistoryTest::empty()
{
    qRegisterMetaType<QModelIndex>("QModelIndex");
    History his;
    connect(&his, SIGNAL(goToIndex(const QModelIndex &)),
            &his, SLOT(currentChanged(const QModelIndex &)));
    QSignalSpy spy(&his, SIGNAL(goToIndex(const QModelIndex &)));
    his.goBackAction = History::backAction(this);
    his.goForwardAction = History::forwardAction(this);
    QCOMPARE(his.goBackAction->isEnabled(), false);
    QCOMPARE(his.goForwardAction->isEnabled(), false);
    his.goBack();
    his.goForward();
    QCOMPARE(spy.count(), 0);
}

Q_DECLARE_METATYPE(QModelIndex);

void HistoryTest::back()
{
    qRegisterMetaType<QModelIndex>("QModelIndex");
    History his;
    his.goBackAction = History::backAction(this);
    his.goForwardAction = History::forwardAction(this);
    connect(&his, SIGNAL(goToIndex(const QModelIndex &)),
            &his, SLOT(currentChanged(const QModelIndex &)));
    QSignalSpy spy(&his, SIGNAL(goToIndex(const QModelIndex &)));

    QDirModel model;
    QModelIndex home = model.index(QDir::homePath());
    QModelIndex desktop = model.index(QDir::homePath() + "/Desktop");
    QModelIndex temp = model.index(QDir::tempPath());

    his.currentChanged(home);
    QCOMPARE(his.goBackAction->isEnabled(), false);
    QCOMPARE(his.goForwardAction->isEnabled(), false);
    his.currentChanged(temp);
    QCOMPARE(his.goBackAction->isEnabled(), true);
    QCOMPARE(his.goForwardAction->isEnabled(), false);
    his.currentChanged(desktop);
    QCOMPARE(spy.count(), 0);

    his.goBack();
    QCOMPARE(his.goBackAction->isEnabled(), true);
    QCOMPARE(his.goForwardAction->isEnabled(), true);
    QCOMPARE(spy.count(), 1);
    QModelIndex idx = (spy.last().first()).value<QModelIndex>();
    QCOMPARE(idx, temp);

    his.goBack();
    idx = (spy.last().first()).value<QModelIndex>();
    QCOMPARE(idx, home);
    QCOMPARE(his.goBackAction->isEnabled(), false);
    QCOMPARE(his.goForwardAction->isEnabled(), true);
    QCOMPARE(spy.count(), 2);

    // nothing should change at this point
    his.goBack();
    QCOMPARE(spy.count(), 2);
    QCOMPARE(his.goBackAction->isEnabled(), false);
    QCOMPARE(his.goForwardAction->isEnabled(), true);
}

void HistoryTest::forward()
{
    qRegisterMetaType<QModelIndex>("QModelIndex");
    History his;
    his.goBackAction = History::backAction(this);
    connect(his.goBackAction, SIGNAL(triggered()), &his, SLOT(goBack()));
    his.goForwardAction = History::forwardAction(this);
    connect(his.goForwardAction, SIGNAL(triggered()), &his, SLOT(goForward()));
    connect(&his, SIGNAL(goToIndex(const QModelIndex &)),
            &his, SLOT(currentChanged(const QModelIndex &)));
    QSignalSpy spy(&his, SIGNAL(goToIndex(const QModelIndex &)));

    QDirModel model;
    QModelIndex home = model.index(QDir::homePath());
    QModelIndex desktop = model.index(QDir::homePath() + "/Desktop");
    QModelIndex temp = model.index(QDir::tempPath());
    his.currentChanged(home);
    his.currentChanged(temp);
    his.currentChanged(desktop);

    his.goBack();
    QCOMPARE(his.goForwardAction->isEnabled(), true);
    QCOMPARE((spy.last().first()).value<QModelIndex>(), temp);

    his.goForward();
    QCOMPARE((spy.last().first()).value<QModelIndex>(), desktop);
    QCOMPARE(his.goBackAction->isEnabled(), true);
    QCOMPARE(his.goForwardAction->isEnabled(), false);
    QCOMPARE(spy.count(), 2);

    his.goBack();
    QCOMPARE((spy.last().first()).value<QModelIndex>(), temp);
    QCOMPARE(his.goBackAction->isEnabled(), true);

    his.goBack();
    QCOMPARE((spy.last().first()).value<QModelIndex>(), home);
    QCOMPARE(his.goBackAction->isEnabled(), false);
    QCOMPARE(his.goForwardAction->isEnabled(), true);
    QCOMPARE(spy.count(), 4);

    // test the actions
    his.goForwardAction->activate(QAction::Trigger);
    QCOMPARE((spy.last().first()).value<QModelIndex>(), temp);
    his.goBackAction->activate(QAction::Trigger);
    QCOMPARE((spy.last().first()).value<QModelIndex>(), home);
    QCOMPARE(spy.count(), 6);

    his.goForward();
    QCOMPARE((spy.last().first()).value<QModelIndex>(), temp);
    his.goForward();
    QCOMPARE((spy.last().first()).value<QModelIndex>(), desktop);

    his.goBack();
    QCOMPARE((spy.last().first()).value<QModelIndex>(), temp);
    his.goBack();
    QCOMPARE((spy.last().first()).value<QModelIndex>(), home);
    his.currentChanged(desktop);
    QCOMPARE(his.goForwardAction->isEnabled(), false);
}

QTEST_MAIN(HistoryTest)
#include "historytest.moc"
