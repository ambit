SOURCES = viewtest.cpp
CONFIG  += qtestlib
CONFIG -= app_bundle

INCLUDEPATH += ../../src/
DEPENDPATH += ../../src/

SOURCES += view.cpp history.cpp iconview.cpp treeview.cpp columnview.cpp
HEADERS += view.h history.h iconview.h treeview.h columnview.h