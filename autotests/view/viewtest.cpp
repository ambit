/**
 * Copyright (C) 2007 Benjamin C. Meyer
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <QtTest/QtTest>
#include <QtGui/QtGui>

#include "view.h"
#include "qfilesystemmodel_p.h"

class ViewTest : public QObject
{
    Q_OBJECT

private slots:
    void empty();
    void mode();
    void rootIndex();
};

void ViewTest::empty()
{
    View view;
    QCOMPARE(QModelIndex(), view.rootIndex());
    QCOMPARE(View::Icon, view.currentMode());
    QCOMPARE(0, ((int)view.currentView()));
    QVERIFY(view.history != 0);
    view.setCurrentMode(View::List);
    view.goUp();
    view.setRootIndex(QModelIndex());
    view.fileOpen();
}

void ViewTest::mode()
{
    View view;
    QFileSystemModel model;
    view.createViews(&model);

    QCOMPARE(View::Icon, view.currentMode());
    view.setCurrentMode(View::List);
    QCOMPARE(View::List, view.currentMode());
    view.setCurrentMode(View::Column);
    QCOMPARE(View::Column, view.currentMode());
    view.setCurrentMode(View::Icon);
    QCOMPARE(View::Icon, view.currentMode());
}

void ViewTest::rootIndex()
{
    qRegisterMetaType<QModelIndex>("QModelIndex");
    View view;
    QFileSystemModel model;
    view.createViews(&model);

    QSignalSpy spy(&view, SIGNAL(currentFolderChanged(const QModelIndex &)));
    QModelIndex home = model.index(QDir::homePath());
    view.setRootIndex(home);
    QCOMPARE(view.rootIndex(), home);
    QCOMPARE(spy.count(), 1);
}

QTEST_MAIN(ViewTest)
#include "viewtest.moc"
